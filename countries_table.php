<?php

require 'bootstrap.php';

$statement = <<<EOS
    CREATE TABLE IF NOT EXISTS countries (
        id INT NOT NULL AUTO_INCREMENT,
        code VARCHAR(255) NOT NULL,
        name VARCHAR(255) NOT NULL,
        PRIMARY KEY (id)
    ) ENGINE=INNODB;

    INSERT INTO countries
        (id, code, name)
    VALUES
        (NULL, '44', 'United Kingdom'),
        (NULL, '7', 'Russia'),
        (NULL, '49', 'Germany'),
        (NULL, '39', 'Italy');
EOS;

try {
    $createTable = $dbConnection->exec($statement);
    echo "Success!\n";
} catch (\PDOException $e) {
    exit($e->getMessage());
}