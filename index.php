<?php

require "bootstrap.php";

use Src\Controller\CountryController;
use Src\Controller\CardController;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET,POST");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode('/', $uri);

if ($uri[1] === 'countries') {
    $countryId = null;
    if (isset($uri[2])) {
        $countryId = (int) $uri[2];
    }

    $requestMethod = $_SERVER["REQUEST_METHOD"];

    $countryController = new CountryController($dbConnection, $requestMethod, $countryId);
    $countryController->processRequest();
} elseif ($uri[1] === 'cards') {
    $cardId = null;
    $action = null;

    if (isset($uri[2]) && is_numeric($uri[2])) {
        $cardId = (int) $uri[2];
        $action = (string) $uri[3];
    }

    if (isset($uri[2]) && !is_numeric($uri[2])) {
        $action = (string) $uri[2];
    }

    $requestMethod = $_SERVER["REQUEST_METHOD"];

    $cardController = new CardController($dbConnection, $requestMethod, $cardId, $action);
    $cardController->processRequest();
} else {
    header("HTTP/1.1 404 Not Found");
    exit();
}

