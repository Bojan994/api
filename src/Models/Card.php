<?php

namespace Src\Models;

class Card
{
    /**
     * @var null
     */
    private $db = null;

    /**
     * Card constructor
     *
     * @param $db
     */
    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Get card by id
     *
     * @param $id
     */
    public function getById(int $id)
    {
        $statement = "SELECT * FROM cards WHERE id = ?;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute([$id]);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    /**
     * Get card balance by id
     * 
     * @param $id
     */
    public function getBalance(int $id)
    {
        $statement = "SELECT balance FROM cards WHERE id = ?;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute([$id]);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        } 
    }

    /**
     * Get card pin by id
     * 
     * @param $id
     */
    public function getPin(int $id)
    {
        $statement = "SELECT pin FROM cards WHERE id = ?;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute([$id]);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        } 
    }
    
    /**
     * Store new card
     *
     * @param array $input
     */
    public function store(Array $input)
    {
        $statement = "INSERT INTO cards
            (first_name, last_name, address, city, country_id, phone, currency, balance, pin, status)
            VALUES
            (:first_name, :last_name, :address, :city, :country_id, :phone, :currency, :balance, :pin, :status);";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute([
                'first_name' => $input['first_name'],
                'last_name'  => $input['last_name'],
                'address' => $input['address'],
                'city' => $input['city'],
                'country_id' => $input['country_id'],
                'phone' => $input['phone'],
                'currency' => $input['currency'],
                'balance' => $input['balance'],
                'pin' => $input['pin'],
                'status' => $input['status'],
            ]);
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }
    
    /**
     * Deactivate card
     *
     * @param int $id
     */
    public function deactivate(int $id)
    {
        $statement = "UPDATE cards SET status = 0 WHERE id = :id;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(['id' => $id]);
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }  
    }
    
    /**
     * Activate card
     *
     * @param int $id
     */
    public function activate(int $id)
    {
        $statement = "UPDATE cards SET status = 1 WHERE id = :id;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(['id' => $id]);
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }  
    }

    /**
     * Update pin
     *
     * @param int $id
     * @param array $input
     */
    public function update(int $id, Array $input)
    {
        $statement = "UPDATE cards SET pin = :pin WHERE id = :id;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute([
                'id' => $id,
                'pin' => $input['pin'],
            ]);
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }  
    }

    /**
     * Load card
     *
     * @param int $id
     * @param array $input
     */
    public function load(int $id, Array $input)
    {
        $statement = "UPDATE cards SET balance = :balance WHERE id = :id;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute([
                'id' => $id,
                'balance' => $input['balance'],
            ]);
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }  
    }
}