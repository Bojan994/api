<?php

namespace Src\Models;

class Country
{
    /**
     * @var null
     */
    private $db = null;

    /**
     * Country constructor
     *
     * @param $db
     */
    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Get all countries
     */
    public function get()
    {
        $statement = "SELECT * FROM countries;";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    /**
     * Get country by id
     *
     * @param $id
     */
    public function getById(int $id)
    {
        $statement = "SELECT * FROM countries WHERE id = ?;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute([$id]);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }
}