<?php

namespace Src\Controller;

use Src\Models\Country;

class CountryController
{
    /**
     * @var $db
     * @var $requestMethod
     * @var $countryId
     * @var $country
     */
    private $db, $requestMethod, $countryId, $country;

    /**
     * CountryController constructor
     *
     * @param $db
     * @param $requestMethod
     * @param $countryId
     */
    public function __construct($db, $requestMethod, $countryId)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->countryId = $countryId;

        $this->country = new Country($db);
    }

    /**
     * Process request with different endpoints
     */
    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->countryId) {
                    $response = $this->getCountriesById($this->countryId);
                } else {
                    $response = $this->getAllCountries();
                };
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    /**
     * Get all countries
     *
     * @return array
     */
    private function getAllCountries()
    {
        $result = $this->country->get();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    /**
     * Get countries by id
     *
     * @param $id
     * @return array
     */
    private function getCountriesById(int $id)
    {
        $result = $this->country->getById($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    /**
     * Not found response
     *
     * @return array
     */
    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}