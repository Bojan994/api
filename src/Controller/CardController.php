<?php

namespace Src\Controller;

use Src\Models\Card;

class CardController
{
    /**
     * @var $db
     * @var $requestMethod
     * @var $cardId
     * @var $card
     * @var $action
     */
    private $db, $requestMethod, $cardId, $card, $action;

    /**
     * CardController constructor
     *
     * @param $db
     * @param $requestMethod
     * @param $cardId
     * @param $action
     */
    public function __construct($db, $requestMethod, $cardId, $action)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->cardId = $cardId;
        $this->action = $action;

        $this->card = new Card($db);
    }

    /**
     * Process request with different endpoints
     */
    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->cardId && $this->action == '') {
                    $response = $this->getCardById($this->cardId);
                } elseif ($this->cardId && $this->action == 'balance') {
                    $response = $this->getCardBalance($this->cardId);
                } elseif ($this->cardId && $this->action == 'pin') {
                    $response = $this->getCardPin($this->cardId);
                }
                break;
            case 'POST':
                if ($this->action == 'create') {
                    $response = $this->createCardFromRequest();
                } elseif ($this->action == 'deactivate') {
                    $response = $this->deactivateCardFromRequest($this->cardId);
                } elseif ($this->action == 'activate') {
                    $response = $this->activateCardFromRequest($this->cardId);
                } elseif ($this->action == 'update') {
                    $response = $this->updateCardPinFromRequest($this->cardId);
                } elseif ($this->action == 'load') {
                    $response = $this->loadCardBalanceFromRequest($this->cardId);
                }
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    /**
     * Get card by id
     *
     * @param $id
     * @return array
     */
    private function getCardById(int $id)
    {
        $result = $this->card->getById($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    /**
     * Get card balance by id
     *
     * @param $id
     * @return array
     */
    private function getCardBalance(int $id)
    {
        $result = $this->card->getBalance($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    /**
     * Get card pin by id
     *
     * @param $id
     * @return array
     */
    private function getCardPin(int $id)
    {
        $result = $this->card->getPin($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    /**
     * Store new card from request
     *
     * @return array
     */
    private function createCardFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validate($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->card->store($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = json_encode(['message' => 'Card created']);
        return $response;
    }

    /**
     * Deactivate card by id
     * 
     * @param int $id
     */
    private function deactivateCardFromRequest(int $id)
    {
        $result = $this->card->deactivate($id);
        if (! $result) {
            return $this->notFoundResponse();
        } else {
            $response['status_code_header'] = 'HTTP/1.1 200 OK';
            $response['body'] = json_encode(['message' => 'Card deactivated']);
            return $response;
        }
    }
    
    /**
     * Activate card by id
     * 
     * @param int $id
     */
    private function activateCardFromRequest(int $id)
    {
        $result = $this->card->activate($id);
        if (! $result) {
            return $this->notFoundResponse();
        } else {
            $response['status_code_header'] = 'HTTP/1.1 200 OK';
            $response['body'] = json_encode(['message' => 'Card activated']);
            return $response;
        }
    }

    /**
     * Update card pin by id
     * 
     * @param int $id
     */
    private function updateCardPinFromRequest(int $id)
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        $result = $this->card->update($id, $input);
        if (! $result) {
            return $this->notFoundResponse();
        } else {
            $response['status_code_header'] = 'HTTP/1.1 200 OK';
            $response['body'] = json_encode(['message' => 'Pin updated']);
            return $response;
        }
    }

    /**
     * Load card balance by id
     * 
     * @param int $id
     */
    private function loadCardBalanceFromRequest(int $id)
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        $result = $this->card->load($id, $input);
        if (! $result) {
            return $this->notFoundResponse();
        } else {
            $response['status_code_header'] = 'HTTP/1.1 200 OK';
            $response['body'] = json_encode(['message' => 'Balance loaded']);
            return $response;
        }
    }

    /**
     * Card input fields validation
     *
     * @param $input
     * @return bool
     */
    private function validate($input)
    {
        if (! isset($input['first_name'])) {
            return false;
        }
        if (! isset($input['last_name'])) {
            return false;
        }
        if (! isset($input['address'])) {
            return false;
        }
        if (! isset($input['city'])) {
            return false;
        }
        if (! isset($input['country_id'])) {
            return false;
        }
        if (! isset($input['phone'])) {
            return false;
        }
        if (! isset($input['currency'])) {
            return false;
        }
        if (! isset($input['balance'])) {
            return false;
        }
        if (! isset($input['pin'])) {
            return false;
        }
        if (! isset($input['status'])) {
            return false;
        }
        return true;
    }

    /**
     * Unprocessable Entity response
     *
     * @return array
     */
    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode(['error' => 'Invalid input']);
        return $response;
    }

    /**
     * Not found response
     *
     * @return array
     */
    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = json_encode(['Message' => 'Not Found']);
        return $response;
    }
}