# api

## Usage

1. Clone this repository and run: composer install
2. Create .env file and set database credentials
3. Run commands for database migrations:
    - php cards_table.php
    - php countries_table.php
4. Launch PHP server (php -S 127.0.0.1:8000) and test API with Postman
